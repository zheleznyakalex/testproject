#1. Создать программу, которая хранит модели электроники в базе данных. Как пример телефоны( модель, производитель , цена, основные характеристики, год выхода)
#2. Наполнить данную базу необходимыми значениями.
#3. Реализовать вывод всей техники, по модели, цене.
#4. Реализовать изменение какого то товара по необходимым значениям
#5. Реализовать удаление товара

import psycopg2

connect = psycopg2.connect(user="postgres", password="1234", host="127.0.0.1", port="5432", database='test_db')
cur = connect.cursor()

cur.execute(
    "CREATE TABLE if not exists elect_db(id int, model varchar(30), name varchar(30), price float(2), system text, release date)")
connect.commit()

def main_menu():
    main_menu_but = int(input("1. Вывод всей техники по названию, модели и цене\n"
                              "2. Изменение товара\n"
                              "3. Удаление товара\n"
                              "Выберите пункт: "))
    if main_menu_but == 1:
        first_main_menu()
    elif main_menu_but == 2:
        second_main_menu()
    elif main_menu_but == 3:
        three_main_menu()
    else:
        print("Ошибка")
        main_menu()


def first_main_menu():
    print("---ВЫВОД ТЕХНИКИ---")
    cur.execute("SELECT name, model, price FROM elect_db ORDER BY id")
    list_items = cur.fetchall()
    for item in list_items:
        print(item)


def second_main_menu():
    print("---ИЗМЕНЕНИЕ ТОВАРА---")
    cur.execute("SELECT * FROM elect_db ORDER BY id")
    items_list = cur.fetchall()
    for item in items_list:
        print(item)
    second_main_menu_but = str(input("Введите id товара, который хотите изменить: "))
    return change_item(second_main_menu_but)


def change_item(numeric):
    cur.execute(f"SELECT * FROM elect_db WHERE id = {numeric}")
    item = cur.fetchone()
    print(item)
    change_item_but = int(input("Что бы вы хотели изменить у данной модели?\n"
                                "1. id\n"
                                "2. Модель\n"
                                "3. Название\n"
                                "4. Цену\n"
                                "5. Основные характеристики\n"
                                "6. Дату выпуска\n"
                                "Выберите раздел: "))
    if change_item_but == 1:
        print("---Изменение id---")
        change_id = int(input("Введите новый id: "))
        cur.execute(f"UPDATE elect_db SET id = {change_id} WHERE id = {numeric}")
        connect.commit()
    elif change_item_but == 2:
        print("---Изменение модели---")
        last_model_name = [*item][1]
        change_model = str(input("Введите новую модель: "))
        cur.execute(
            f"UPDATE elect_db SET model = '{change_model}' WHERE model = '{last_model_name}' AND id = {numeric}")
        connect.commit()
    elif change_item_but == 3:
        print("---Изменение названия---")
        last_name = [*item][2]
        change_name = str(input("Введите новое имя: "))
        cur.execute(f"UPDATE elect_db SET name = '{change_name}' WHERE name = '{last_name}' AND id = {numeric}")
        connect.commit()
    elif change_item_but == 4:
        print("---Изменение цены---")
        last_price = [*item][3]
        change_price = float(input("Введите новую цену: "))
        cur.execute(f"UPDATE elect_db SET price = '{change_price}' WHERE price = '{last_price}' AND id = {numeric}")
        connect.commit()
    elif change_item_but == 5:
        print("---Изменение основных характеристик---")
        last_system = [*item][4]
        last_system = "".join(last_system)
        last_system = "{" + last_system + "}"
        change_system = str(input("Введите основные характеристики"
                                  "\n"))
        cur.execute(
            f"UPDATE elect_db SET system = '{change_system}' WHERE system = '{last_system}' AND id = {numeric}")
        connect.commit()
    elif change_item_but == 6:
        print("---Изменение даты выпуска---")
        last_release = [*item][5]
        change_release = str(input("Введите новую дату: "))
        cur.execute(
            f"UPDATE elect_db SET release = '{change_release}' WHERE release = '{last_release}' AND id = {numeric}")
        connect.commit()


def three_main_menu():
    print("---УДАЛЕНИЕ ТОВАРА---")
    cur.execute("SELECT id, name, model, price FROM elect_db ORDER BY id")
    list_items = cur.fetchall()
    for item in list_items:
        print(item)
    three_main_menu_but = str(input("Введите id товара: "))
    return item_del(three_main_menu_but)


def item_del(numeric):
    cur.execute(f"DELETE FROM elect_db WHERE id = {numeric}")
    connect.commit()
    print("Информация успешно удалена")
    main_menu()


main_menu()

connect.close()
