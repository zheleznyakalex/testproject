def numFibonacci():
    num_f = [1, 1]
    result = 1
    while num_f[-1] < 2_000_000:
        if len(num_f) % 3 == 0:
            result *= num_f[-1]
        num_f.append(num_f[-1] + num_f[-2])
    print(result)

numFibonacci()
