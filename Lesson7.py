#bool - логический тип (True, False)
print(type(True))

#int  - целое число
print(type(7))

#float - число с плавающей точкой (дробное)
print(type(0.5))

#str - строка

print(type('True'))

#NoneType - объект со значением None
print(type(None))

#complex -  комплексные числа
print(type(2+2j))

#bytes - неизменяемая последовательность байтов
print(type(bytes("Строка", "utf-8")))

#bytearray - изменяемая последовательность байтов
print(type(bytearray("Строка", "utf-8")))

#list - список
print(type([1,2,3]))

#tuple - кортеж
print(type((1,2,3)))

#range - диапазон
print(type(range(1, 10)))

#dict - словари
#a - это ключ, 5 - это значение
print(type({'a': 5, 'b': 10}))

#a = [1,2,3]
#print(a[1])
#a = [[1,2,3],[1,2,3],[1,2,3]]
#print(a[1][1])

#set - множества (удаляет все дубли)
print(type({'a','b','c'}))