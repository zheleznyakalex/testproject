#tuple1 = ('1',)
# без запятой тип был бы  str
#print(type(tuple1))

tuple1 = ('Python language')
for char in tuple1:
    print(char)
# перебор всего кортежа
print(len(tuple1))

a = 5
b = 10
a, b = b, a
#перемена местами

