#doctest урок 18

import Homework_lesson17
import unittest

class MyTestProg(unittest.TestCase):

    def setUp(self) -> None:
        self.month = "qwe"

    def test_len_month(self):
        """Количество месяцев равно 12?"""
        self.assertEquals(len(list(Homework_lesson17.months.keys())), 12)

    def test_month(self):
        """Для каждого месяца содержится по 2 значения?"""
        for key in list(Homework_lesson17.months.keys()):
            self.assertEquals(len(Homework_lesson17.months[key]), 2)

    def test_month_in(self):
        """Месяц находится в базе данных?"""
        self.assertFalse(Homework_lesson17.month_in(self.month))

    def test_difference_watch_data(self):
        """Функция watch_data правильно проводит вычисления?"""
        self.assertAlmostEqual('1.11', Homework_lesson17.watch_data('3.33', '2.22'))

    def test_difference_func(self):
        """Функция difference работает корректно?"""
        self.assertAlmostEqual('1.11', Homework_lesson17.difference(3.33, 2.22))
