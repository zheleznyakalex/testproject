#Написать программу спрашивающего у пользователя его доход, расход.
#Реализовать функционал вывода дохода, расхода, остатка денежных средст за месяц, 3, 6 и 12 месяцев.
#Данные сохранять в файл. Также реализовать возможность загрузки данных из файла.
#Прописать все необходимые тесты

import sys

months = {}
with open('data.txt', encoding='utf8') as f:
    for line in f:
        key, *value = line.split()
        months[key] = value


def main_menu():
    while True:
        print('1. Ввести новые данные'
              '\n2. Вывести результирующие данные'
              '\n3. Закрыть программу')
        main_menu_num = int(input('Выберите раздел меню: '))
        if main_menu_num == 1:
            first_main_menu()
        elif main_menu_num == 2:
            two_main_menu()
        elif main_menu_num == 3:
            sys.exit('Всего доброго!')
        else:
            print('Ошибка. Повторите попытку!')


def first_main_menu():
    print('---Новые данные---')
    while True:
        month_in = str(input('Введите месяц: '))
        if month_in in list(months.keys()):
            print(f'На данный момент'
                  f'\nДоход: {months[month_in][0]}'
                  f'\nРасход: {months[month_in][1]}')
            new_income = float(input('Введите новый доход: ').strip().replace(',', '.'))
            new_cost = float(input('Введите новый расход: ').strip().replace(',', '.'))
            value = [(month_in, [str(new_income), str(new_cost)])]
            months.update(value)
            save_data()
            logic = int(input('\n1. Продолжить вводить данные'
                              '\n2. Выйти в главное меню'
                              '\nВведите значение: '))
            if logic == 1:
                first_main_menu()
            elif logic == 2:
                main_menu()
            else:
                print('Ошибка! Выход в главное меню')
                main_menu()
        else:
            print('Ошибка! Повторите попытку')


def two_main_menu():
    while True:
        print('---База данных---'
              '\n1. Результаты за месяц'
              '\n2. Результаты за 3 месяца'
              '\n3. Результаты за 6 месяцев'
              '\n4. Результаты за год'
              '\n5. Выйти в главное меню')
        result_in = int(input('Выберите раздел: '))
        if result_in == 1:
            print('---Результаты за месяц---')
            month_in = str(input('Введите месяц: '))
            if month_in in list(months.keys()):
                print(f'Доход: {months[month_in][0]}'
                      f'\nРасход: {months[month_in][1]}'
                      f'\nОстаток денежных средств: {format(float(months[month_in][0]) - float(months[month_in][1]), ".2f")}')
        elif result_in == 2:
            print('--Результаты за 3 месяца--'
                  '\nС января по март')
            result_income = 0
            result_cost = 0
            for month, money in months.items():
                if month == 'апрель':
                    break
                else:
                    result_income += float(months[month][0])
                    result_cost += float(months[month][1])
            print(f'Доход: {format(result_income, ".2f")}'
                  f'\nРасход: {format(result_cost, ".2f")}'
                  f'\nОбщий остаток: {format(result_income - result_cost, ".2f")}')
        elif result_in == 3:
            print('---Результаты за 6 месяцев---'
                  '\nС января по июнь')
            result_income = 0
            result_cost = 0
            for month, money in months.items():
                if month == 'июль':
                    break
                else:
                    result_income += float(months[month][0])
                    result_cost += float(months[month][1])
            print (f'Доход: {format (result_income, ".2f")}'
                   f'\nРасход: {format (result_cost, ".2f")}'
                   f'\nОбщий остаток: {format (result_income - result_cost, ".2f")}')
        elif result_in == 4:
            print('---Результаты за год---')
            result_income = 0
            result_cost = 0
            for month, money in months.items():
                result_income += float(months[month][0])
                result_cost += float(months[month][1])
            print (f'Доход: {format (result_income, ".2f")}'
                   f'\nРасход: {format (result_cost, ".2f")}'
                   f'\nОбщий остаток: {format (result_income - result_cost, ".2f")}')
        elif result_in == 5:
            main_menu()
        else:
            print('Ошибка! Повторите попытку')


def save_data():
    file = open('data.txt', 'w', encoding='utf8')
    for key1, value1 in months.items():
        in_line = f"{key1} {' '.join(value1)}\n"
        file.write(in_line)
    file.close()
    return save_data

def month_in(result):
    if result in list(months.keys()):
        return True
    else:
        return False

def watch_data(a, b):
    """
    >>> watch_data('3.33', '2.22')
    Доход: 3.33
    Расход 2.22
    Остаток 1.11
    '1.11'

    """
    print(f'Доход: {a}'
          f'\nРасход {b}'
          f'\nОстаток {format(float(a) - float(b), ".2f")}')
    q = format(float(a) - float(b), '.2f')
    return q

def difference(a, b):
    """
    Функция находит разницу между доходом и расходом за весь период
    >>> difference(3.33, 2.22)
    '1.11'

    """
    return format(a - b, '.2f')



if __name__ == '__main__':
    main_menu()
