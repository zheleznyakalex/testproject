import Homework_lesson17
import unittest

class MyTestProg(unittest.TestCase):
    def test_len_month(self):
        self.assertTrue(len(list(Homework_lesson17.months.keys())), 12)

    def test_month_in(self):
        for key in list(Homework_lesson17.months.keys()):
            self.assertEquals(len(Homework_lesson17.months[key]), 2)
