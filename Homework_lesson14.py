import time

class timer:
    def __init__(self, func):
        self.func = func
        self.alltime = 0

    def __call__(self, *args, **kwargs):
        start_time = time.time()
        original_result = self.func(*args, **kwargs)
        result_time = time.time() - start_time
        self.alltime += result_time
        print(f'Общее время выполнения функции {self.func.__name__} '
              f'составило {self.alltime}. '
              f'\nВремя выполнения конкретного шага: {result_time}')
        return original_result


@timer
def listcomp(N):
    return [x ** 2 for x in range(1, N + 1)]


@timer
def mapcall(N):
    return list(map((lambda x: x ** 2), range(1, N + 1)))


result = listcomp(3)
print(result)
listcomp(1000000)
print(f'Результирующее время функции: {listcomp.alltime}')

result = mapcall(3)
print(result)
mapcall(1000000)
print(f'Результирующее время функции: {mapcall.alltime}')


@timer
class MyClass:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.result = self.x * self.y


x = MyClass(4, 4)
print(x.result)

