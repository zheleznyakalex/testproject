sum_coupe = 9
seatplace_coupe = 4


logic = True
while logic:
    coupe = int(input('Введите номер своего места: '))
    in_seatplace = 1
    for i in range(1, coupe):
        if i % seatplace_coupe == 0:
            in_seatplace += 1
    logic = False

    if in_seatplace > sum_coupe:
        print(f'Неверно! Доступно {sum_coupe} купе')
        logic = True
    else:
        print(f'Номер вашего купе: {in_seatplace}')
