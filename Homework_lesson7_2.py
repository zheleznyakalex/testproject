while True:
    num = input("Введите любое число от 1 до 2000: ")
    if num.isdigit():
        if int(num) % 10 == 1 and int(num) % 100 != 11:
            print(f'{num} кот')
        elif int(num) % 10 <= 4 and int(num) >= 2 and (int(num) % 100 < 10 or int(num) % 100 > 20) and int(num) % 10 !=0:
            print(f'{num} кота')
        else:
            print(f'{num} котов')
    else:
        print('Неверно!')