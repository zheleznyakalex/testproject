import random
secret_number = random.randint(1, 11)
status = True
while status:
    user_number = int(input('Input Number: '))
    if secret_number > user_number:
        print('Secret number >')
    elif secret_number < user_number:
        print('Secret number <')
    else:
        print('Secret number is correct. You win!')
        status = False
