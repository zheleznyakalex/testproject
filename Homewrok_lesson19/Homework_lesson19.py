# 1. Создать базу данных хранящую информацию о ФИО, должности и заработной плате.
# 2. Наполнить ее

import sqlite3

con = sqlite3.connect('employee.db')
cur = con.cursor()
cur.execute('CREATE TABLE if not exists employee(ID integer PRIMARY KEY, name text, post text, salary integer)')
employee_list = [
    (1, 'Никифоров А.А.', 'Директор', 120.000),
    (2, 'Шпатлев Б.Б.', 'Заместитель директора', 90.000),
    (3, 'Павич В.В.', 'Старший менеджер', 75.000),
    (4, 'Цуканов Г.Г.', 'Менеджер', 50.000)]
cur.executemany("INSERT INTO employee VALUES(?,?,?,?)", employee_list)
con.commit()
con.close()
