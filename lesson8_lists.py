#список - изменяемый и упорядоченный тип данных
name = [True, 1, 'hello', 2.5]
#range - возвращает последовательность, len - узнаёт длину последовательности
for i in range(len(name)):
#    print(i)
    print(name[i])

list1 = ['apple', 'orange']
list1.append('banana')
#print(list1)
#list1.remove('apple')
#print(list1)
#fruit = list1.pop(1)
#print(fruit)
list1.extend(['tomato', 'peach'])
#print(list1)
print(list1.index('apple'))

num_list = [1, 2, 3, 1, 4, 2]
result =[]
for i in num_list:
    if num_list.count(i) > 1 and i not in result:
        result.append(i)
print(result)