goods = {"яблоки": 5.780, "апельсины": 12.040}
logic = True
while logic:
    print('Отгрузка\n'
          'Погрузка\n'
          )
    # Пользователь выбирает пункт из меню
    menu = input('Выберите действие: ')
    if menu == 'Отгрузка':
        print('\n' * 3, '-----Отгрузка-----')
        logic1 = True
        while logic1:
            goods_name = str(input('Номенклатура товара: '))
            if goods_name in goods.keys():
                print(f'Товар {goods_name} доступно: {goods[goods_name]} кг')
                goods_quantity = float(input('Введите количество отгружаемого товара: '))
                goods[goods_name] -= goods_quantity
                print(f'Товар {goods_name} будет отгружен со склада в количестве {goods_name} кг.\n'
                      f'Остаток товара {goods_name} составляет {goods[goods_name]} кг.'
                      f'\nПродолжить отгрузку?')
                if goods_quantity >= 0:
                    continue
                else:
                    print('Ошибка! Товар не может принимать отрицательные значения!')
                    break
            else:
                print('Ошибка! Данный товар не обнаружен!')
            menu1 = str(input('Да\n'
                              'Нет\n'
                              'Выберите действие: '))
            if menu1 == 'Да':
                pass
            elif menu1 == 'Нет':
                print("\n" * 3)
                logic1 = False
                break
            else:
                print("Ошибка!")
    elif menu == 'Погрузка':
        print('\n' * 3, "-----Погрузка-----")
        logic2 = True
        while logic2:
            goods_name = str(input('Номенклатура товара: '))
            goods_quantity = float(input('Количество загружаемого товара: '))
            if goods_quantity in goods:
                goods[goods_name] += goods_quantity
            else:
                goods[goods_name] = goods_quantity
            print(f'Товар {goods_name} будет доставлен на склад в количестве {goods_quantity} кг.\n'
                  f'Количество товара {goods_name} после погрузки составит {goods[goods_name]} кг.\n'
                  f'Продолжить погрузку товара?\n')
            menu2 = str(input('Да\n'
                              'Нет\n'
                              'Выберите действие: '))
            if menu2 == 'Да':
                pass
            elif menu2 == 'Нет':
                print('\n' * 3)
                logic2 = False
                break
            else:
                print('Ошибка!')



