def natural_num(n):
    list1 = []
    list2 = []
    num = int(n)
    for i in range(1, num + 1):
        list1.append(i)
        list2.append(i ** 2)
    result1 = sum(list1) ** 2
    result2 = sum(list2)
    return result1 - result2

print(natural_num(100))
