def Calculator(x):
    global num1, num2
    logic = True
    while logic:
        if type(x) == int:
            num1 = x
        else:
            try:
                num1 = int(input('Введите первое число: '))
            except ValueError:
                print('Ошибка! Вводите только целые числа!')
                Calculator('a')
        operation = str(input("Введите оператор: ")).strip()
        try:
            num2 = int(input('Введите второе число: '))
        except ValueError:
            print('Ошибка! Вводите только целые числа!')
            Calculator('a')
        if operation == '+':
            print('Ответ:', int(result := num1 + num2))
            Logics(result)
            logic = False
        elif operation == '/':
            try:
                print('Ответ:', int(result := num1 / num2))
            except ZeroDivisionError:
                print(result := 'Ошибка! Деление на ноль невозможно.')
            Logics(result)
            logic = False
        elif operation == '-':
            print('Ответ:', int(result := num1 - num2))
            Logics(result)
            logic = False
        elif operation == '*':
            print('Ответ:', int(result := num1 * num2))
            Logics(result)
            logic = False
        else:
            print('Введен неверный оператор, убедитесь в правильности введённых данных')

def Logics(x):
    q1 = input('Продолжить вычисления c вычисленным числом? y - Да, n - Нет'
               '\n')
    if q1 == 'y':
        if type(x) == str or float:
            return print('Ошибка! Работа с данным числом невозможна.')
        else:
            Calculator(int(x))
    elif q1 == 'n':
        q2 = input('Начать вычисления заново ? y - Да'
                   '\nДля выхода из приложения нажмите любую кнопку'
                   '\n')
        if q2 == 'y':
            Calculator('a')
        else:
            return print('Всего хорошего!')
    else:
        print('Ошибка!')
        Logics(x)

Calculator('a')
