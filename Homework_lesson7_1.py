while True:
    first_name = input('Введите имя: ')
    second_name = input('Введите фамилию: ')
    age = input('Введите возраст: ')
    if first_name.isalpha() and  first_name.istitle() and second_name.isalpha() and second_name.istitle():
        print('Всё верно')
        if age.isdigit() and 0 <= int(age) < 130:
            break
        else:
            print ('Вы ввели неправильный возраст!')
            continue
    else:
        print('Неверные данные!')
print(f'Ваше имя {first_name} {second_name}, ваш возраст: {age}')
